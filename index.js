// number 3
let myNumber = Number(prompt("Enter your number:"))

// number 4
while (myNumber !== 0){
	console.log(myNumber);
	myNumber--;
}

// number 5,6 and 7
let myNumber2 = Number(prompt("Enter your number:"))
console.log(`The number you provided is ${myNumber2}`)

for(let index = myNumber2; index >= 50; index--){

 if(index % 10 === 0){
 	console.log("The number is divisible by 10. Skipping the number")
	continue
    }
    if(index % 5 === 0){
    	console.log(index)
    	continue	
	}
	if(index < 50){
		
		break
    }
}
console.log(`The current value is at 50. Terminating the loop`)

// number 8,9 and 10

let myString = "supercalifragilisticexpialidocious"
console.log(myString)
let myString2 = ""


for(let x = 0; x < myString.length; x++){
	if(
		myString[x].toLowerCase() === "a" ||
		myString[x].toLowerCase() === "e" ||
		myString[x].toLowerCase() === "i" ||
		myString[x].toLowerCase() === "o" ||
		myString[x].toLowerCase() === "u"
		){
		continue
	}else{
		myString2 = myString2 + myString[x]
	}
}
console.log(myString2)
